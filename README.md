**Primer entrega**
Trevor Fernandez

**Bienvenida express**
http://localhost:3000/express

**ABM de bicicletas**
http://localhost:3000/bicicletas



DESDE POSTMAN

**Coleccion de Bicicletas (GET)**
http://localhost:3000/api/bicicletas

**Crear Bicicleta (POST)**
http://localhost:3000/api/bicicletas/create

**Modificar Bicicleta (POST)**
http://localhost:3000/api/bicicletas/update

**Borrar Bicicleta (DELETE)**
http://localhost:3000/api/bicicletas/delete